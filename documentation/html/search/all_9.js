var searchData=
[
  ['main_57',['main',['../snake_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'snake.c']]],
  ['map_5fphys_5faddress_58',['map_phys_address',['../mzapo__phys_8c.html#a3925c5fab561521513a23e5f7ede0c9e',1,'map_phys_address(off_t region_base, size_t region_size, int opt_cached):&#160;mzapo_phys.c'],['../mzapo__phys_8h.html#a3925c5fab561521513a23e5f7ede0c9e',1,'map_phys_address(off_t region_base, size_t region_size, int opt_cached):&#160;mzapo_phys.c']]],
  ['map_5fphys_5fmemdev_59',['map_phys_memdev',['../mzapo__phys_8c.html#a817f2c47e0b9290c9d64751b49354c83',1,'mzapo_phys.c']]],
  ['maxwidth_60',['maxwidth',['../structfont__descriptor__t.html#a8e21a73982523c96f1ca5f39487956a7',1,'font_descriptor_t']]],
  ['mzapo_5fparlcd_2ec_61',['mzapo_parlcd.c',['../mzapo__parlcd_8c.html',1,'']]],
  ['mzapo_5fparlcd_2eh_62',['mzapo_parlcd.h',['../mzapo__parlcd_8h.html',1,'']]],
  ['mzapo_5fphys_2ec_63',['mzapo_phys.c',['../mzapo__phys_8c.html',1,'']]],
  ['mzapo_5fphys_2eh_64',['mzapo_phys.h',['../mzapo__phys_8h.html',1,'']]],
  ['mzapo_5fregs_2eh_65',['mzapo_regs.h',['../mzapo__regs_8h.html',1,'']]]
];
