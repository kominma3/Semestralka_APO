var searchData=
[
  ['parlcd_5fdelay_71',['parlcd_delay',['../mzapo__parlcd_8c.html#afef12925f598e36664d1d3b6de27c881',1,'parlcd_delay(int msec):&#160;mzapo_parlcd.c'],['../mzapo__parlcd_8h.html#afef12925f598e36664d1d3b6de27c881',1,'parlcd_delay(int msec):&#160;mzapo_parlcd.c']]],
  ['parlcd_5fhx8357_5finit_72',['parlcd_hx8357_init',['../mzapo__parlcd_8c.html#a37a4ee10b3cd7f3b1282d944f70c591b',1,'parlcd_hx8357_init(unsigned char *parlcd_mem_base):&#160;mzapo_parlcd.c'],['../mzapo__parlcd_8h.html#a37a4ee10b3cd7f3b1282d944f70c591b',1,'parlcd_hx8357_init(unsigned char *parlcd_mem_base):&#160;mzapo_parlcd.c']]],
  ['parlcd_5freg_5fbase_5fphys_73',['PARLCD_REG_BASE_PHYS',['../mzapo__regs_8h.html#ae34ff3f28093ee8dce3048aeecddeb37',1,'mzapo_regs.h']]],
  ['parlcd_5freg_5fcmd_5fo_74',['PARLCD_REG_CMD_o',['../mzapo__regs_8h.html#aaec731d1269f911eebe67a27d4fd2e70',1,'mzapo_regs.h']]],
  ['parlcd_5freg_5fdata_5fo_75',['PARLCD_REG_DATA_o',['../mzapo__regs_8h.html#ad110747a1220ac55aadc403efc501b8f',1,'mzapo_regs.h']]],
  ['parlcd_5freg_5fsize_76',['PARLCD_REG_SIZE',['../mzapo__regs_8h.html#a86f33c44ef575912f459a11807e0e06d',1,'mzapo_regs.h']]],
  ['parlcd_5fwrite_5fcmd_77',['parlcd_write_cmd',['../mzapo__parlcd_8c.html#ab30b1b616caf5991cd8e1a6a52ba23e7',1,'parlcd_write_cmd(unsigned char *parlcd_mem_base, uint16_t cmd):&#160;mzapo_parlcd.c'],['../mzapo__parlcd_8h.html#ab30b1b616caf5991cd8e1a6a52ba23e7',1,'parlcd_write_cmd(unsigned char *parlcd_mem_base, uint16_t cmd):&#160;mzapo_parlcd.c']]],
  ['parlcd_5fwrite_5fdata_78',['parlcd_write_data',['../mzapo__parlcd_8c.html#ab9125a7610ba0302c004802fb908590d',1,'parlcd_write_data(unsigned char *parlcd_mem_base, uint16_t data):&#160;mzapo_parlcd.c'],['../mzapo__parlcd_8h.html#ab9125a7610ba0302c004802fb908590d',1,'parlcd_write_data(unsigned char *parlcd_mem_base, uint16_t data):&#160;mzapo_parlcd.c']]],
  ['parlcd_5fwrite_5fdata2x_79',['parlcd_write_data2x',['../mzapo__parlcd_8c.html#a688f82186392823303897ee3f270718a',1,'parlcd_write_data2x(unsigned char *parlcd_mem_base, uint32_t data):&#160;mzapo_parlcd.c'],['../mzapo__parlcd_8h.html#a688f82186392823303897ee3f270718a',1,'parlcd_write_data2x(unsigned char *parlcd_mem_base, uint32_t data):&#160;mzapo_parlcd.c']]],
  ['point_5fx_80',['point_x',['../snake_8c.html#af876319c1fa1aaea3ae3ff843635c9f2',1,'snake.c']]],
  ['point_5fy_81',['point_y',['../snake_8c.html#a6c19d968f50eed5a6617df2ac5a1d7a1',1,'snake.c']]],
  ['print_5flcd_82',['print_lcd',['../snake_8c.html#aaf6db7de2eb67edb55c5a5fb8bbd05bc',1,'snake.c']]]
];
