var searchData=
[
  ['change_5fdirection_10',['change_direction',['../snake_8c.html#a2adf07c7222d9fd6473a1a64b57e7886',1,'snake.c']]],
  ['char_5fwidth_11',['char_width',['../snake_8c.html#ad04a5c1a4d66e8f6b4fdef99609ce5fc',1,'snake.c']]],
  ['check_5fcollision_12',['check_collision',['../snake_8c.html#a6a383bb6deaade583631c82cd177fca1',1,'snake.c']]],
  ['check_5ffood_13',['check_food',['../snake_8c.html#a86106977be95aca3b0ab2b9fb661fa1a',1,'snake.c']]],
  ['check_5fpossible_5fcollision_14',['check_possible_collision',['../snake_8c.html#aa2ff2aa01a4d62937936e32363cfb416',1,'snake.c']]],
  ['colour_15',['colour',['../structsnake__t.html#a6f20169980905bd54096a04f8a3a7b02',1,'snake_t']]]
];
