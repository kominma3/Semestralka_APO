var indexSectionsWithContent =
{
  0: "_abcdefhlmnopstwxy",
  1: "fst",
  2: "fms",
  3: "cdemnpw",
  4: "abcdfhlmnopswxy",
  5: "f",
  6: "_adlpst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros"
};

