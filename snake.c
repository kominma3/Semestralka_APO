/*******************************************************************
 Snake program

  snake.c       - main and only file

  (C) Copyright 2020 by Martin Komínek and Adéla Kubíková
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/
#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"


#define SCALE_PIXEL 4
#define SIZE_SQUARE 20
#define TEXT_SPACE 60
#define LINE_SIZE 10

unsigned short *fb;
uint16_t lcd_display[320][480];/*!< 2d array of 16 bits numbers, that holds information about each pixel and colour they should print. You simply choose pixel you want to write, choose colour and then you have to render display*/
font_descriptor_t* fdes = &font_winFreeSystem14x16;
int point_x;/*!< x Cordinates of food position*/
int point_y;/*!< y Cordinates of food position*/
int number_of_winner = 0;
/**
 * structure, that represents one tile of snake
 * we always free tile if it is one longer than length of snake
 * structure has 3 value
 * @param x_axis Position of tile on x axis
 * @param y_axis Position of tile on y axis
 * @param next pointer to next tile to connect the snake and keep the order
*/
typedef struct
{
	int x_axis;
	int y_axis;
	void *next;
}tile_t;
/**
 * structure that holds all necessary information about snake
 * @param length keeps information about lenth of snake to know how many tiles we should display and works as score at the same time
 * @param first is a pointer to first tile of snake, from which we already have other pointers
 * @param x_move has value -1,0,1 depending on a direction we are heading. this value is always changed together with y_move and is counted from direction
 * @param y_move has value -1,0,1 depending on a direction we are heading. this value is always changed together with x_move and is counted from direction
 * @param direction\ has value between 1 to 4 and we set value of x_move and y_move using this param
 * @param led is an adress into memory that we use to indicate of current state of snake
 * @param AI is a simle boolean that tells us, if we want to control snake automatically or manually via ssh
 * @param colour is a number that tells us in which coulour we want to set snake
 */
 
	typedef struct
	{
		int length;
		tile_t *first;
		int x_move;
		int y_move;
		int direction;
		unsigned char *led;
		bool AI;
		uint16_t colour;
	}snake_t;
	
	
/**
 * Fuction to draw a square of scale size
 * @param x tells us x coordinate on lcd_display we want to start drawing a square
 * @param y tells us y coordinate on lcd_display we want to start drawing a square
 * @param colour indicates colour we will write on lcd_display
 * @param scale sets length of squere we want to write so for instance if scale = 5 than we will write 5x5 square
*/
void draw_scale(int x, int y, unsigned short colour, int scale) {
  if (x>=0 && x<480 && y>=0 && y<320) {
    for(int i = 0; i < scale; i ++)
    {
		for(int j = 0 ; j < scale; j++)
		{
			lcd_display[y+i][x+j] = colour;
		}
	}
  }
}
/**
 * gets a width of any char in our font
 * @param takes adress of font descriptor on which we have necessary information about font
 * @param ch is a character that we want to print, using our font
*/
int char_width(font_descriptor_t* fdes, int ch) {
  int width = 0;
  if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
    ch -= fdes->firstchar;
    if (!fdes->width) {
      width = fdes->maxwidth;
    } else {
      width = fdes->width[ch];
    }
  }
  return width;
}
/**
 * writes colour into led diod
 * @param led is an adress in memory that we want to write our colour
 * @param coulour is a 32 bits number with information about colour we want to display
*/
void write_led(unsigned char *led,uint32_t colour)
{
	*(volatile uint32_t*)(led) =  colour;	
}
/**
 * function that reads definition of char and decides which pixels will be printed
 * @param x positon on x_axis of display where we will start printing char
 * @param y positon on y_axis of display where we will start printing char
 * @param fdes is an adress where we have definition of our font
 * @param ch is a char we want to print
 * @param c is a colout in which we want to print our text 
 * @param scale tells as length of square that will represent each pixel
 */
void draw_char(int x, int y, font_descriptor_t* fdes, char ch,unsigned short c,int scale) {
	if((ch >= fdes->firstchar) && (ch- fdes->firstchar < fdes->size))
	{
		int bw =(fdes->maxwidth+15)/16;
		uint16_t *ptr;
		if(fdes->offset)
		{
			ptr = &fdes->bits[fdes->offset[ch]];
		}
		else
		{
			ptr = fdes->bits + (ch-fdes->firstchar)*bw*fdes->height;
		}
		int w = char_width(fdes,ch);
		for(int i = 0; i < fdes->height;i++)
		{
			uint16_t val = *(ptr++);
			for(int j = 0; j < w; j ++)
			{
				if((val & 0x8000) != 0)
				{
					draw_scale(x+j*scale, y+i*scale, c,scale);
					
				}
				val <<=1;
			}
		}
	}
}
/**
 * draws a snake on lcd display
 * It takes each tile of snake and print it on lcd display
 * @param snake is a pointer on snake we want to print
*/
void write_snake(snake_t *snake)
{
	int counter = 1;
	tile_t *temporary = snake->first;
	while(temporary != NULL)
	{
		for(int i = 0; i < SIZE_SQUARE;i++)
		{	
			for(int j= 0; j< SIZE_SQUARE;j++)
			{
				lcd_display[temporary->x_axis*SIZE_SQUARE+i+ LINE_SIZE][temporary->y_axis* SIZE_SQUARE+ j+ LINE_SIZE] = snake->colour;
			}
		}
		//printf("%d ",counter);
		if(counter == snake->length)
		{
			free(temporary->next);
			temporary->next = NULL;
			break;
		}
		counter++;
		temporary = temporary->next;
	}
}
/**
 * prints white screen into window where snake was and sets led to red because of a colission
 */
void end_screen(snake_t *snake)
{   //red led
	write_led(snake->led,0x00ff0000);
	for(int i = 0+ LINE_SIZE; i <320-TEXT_SPACE- 2 * LINE_SIZE; i++)
	{
		for(int j=0+ LINE_SIZE; j< 480- 2 * LINE_SIZE; j++)
		{
			lcd_display[i][j] = 0xffff;
		}
	}
}

/**
 * takes array of chars and draws it char by char
 * @param ch is a pointer to start of string we want to print
 * @param length is a number of chars we want to print
 * @param colour is a param in which colour will be the string printed
 * @param x is a positon of start point where will be the string printed
 * @param y is a position of start point where will be the string printed
 * @param scale scales one piel to a square of length of scale
 * @param space is a length in pixels we will add between char on x axis
*/
void draw_text(char *ch,int length,uint16_t colour,int x,int y,int scale,int space)
{
	for (int i=0; i< length; i++)
	{
		draw_char(x, y, fdes, *ch,colour,scale);
		x+=char_width(fdes, *ch)*scale + space;
		ch++;
	}
}
/**
 * function that checkes if snake eated food
 * if snake eats it we add 1 to its length and we generate coordinates of a new food
 * @param snake is a structure that holds all information and we need to check its first tile and its position
*/
void check_food(snake_t *snake)
{
	if(point_x == (snake->first)->x_axis && point_y == (snake->first)->y_axis)
	{   //blue led - eaten food
		write_led(snake->led,0x000000ff);
		snake->length +=1;
		point_x = rand() % ((320 - TEXT_SPACE - 2*LINE_SIZE) / SIZE_SQUARE);
		point_y = rand() % ((480- 2*LINE_SIZE) / SIZE_SQUARE);
	}
}
/**
 * We copy values in the buffer into lcd buffer
 * @param parlcd_mem_base is an adress where memory of lcd display actualy is
*/
void print_lcd(unsigned char *parlcd_mem_base)
{
	parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int i = 0; i < 320 ; i++) {
        for (int j = 0; j < 480 ; j++) {
            parlcd_write_data(parlcd_mem_base, lcd_display[i][j]);
        }
    }
}
/**
 * add another tile in the direction of next move of snake
 * @param snake it takes information about snake and in the direction snake is pointed it creates a new tile of snake and connects it with a snake
*/
void next_move(snake_t *snake)
{
	tile_t*tile = malloc(sizeof(tile_t));
	tile->x_axis =(snake->first)->x_axis + snake->x_move;
	tile->y_axis = (snake->first)->y_axis + snake->y_move;
	tile->next = snake->first;
	snake->first = tile;
}
/**
 * function that contols if one snake collided with a boarder or snake(enemy of with itself)
 * @param snake is an obstacle where we check if a new tile doesn colide with snake
 * @param tile is a new tile we want to add and we have to check if it is not incorrect move, so within boarders and cannot colide with both own snake and enemy snake
 * @param x_limit is a size of play area on x_axis
 * @param y_limit is a size of play ate on y_axis
 * @param check_first sets if we control first tile of snake, becouse if we control collision within snake we dont want to conrol tile with itself
*/
bool check_collision(snake_t *snake,tile_t *tile,int x_limit,int y_limit,bool check_first)
{
		if(tile->x_axis >= x_limit +1 || tile->y_axis >= y_limit +1 || tile->x_axis < 0 || tile->y_axis < 0)
		{
			return true;
		}
		tile_t *temporary;
		if(check_first==true)
		{
			temporary = snake->first;
		}
		else
		{
			temporary = (snake->first)->next;
		}
		int counter = 1;
		while(temporary!= NULL)
		{
			if(tile->x_axis== temporary->x_axis &&  tile->y_axis== temporary->y_axis)
			{
				return true;
			}
			if(counter == snake->length)
			{
				break;
			}
			temporary = temporary->next;
			counter++;
		}
		return false;
}
/** 
 * Function that AI uses to decide if next tile is available to avoid collisions with itself or enemy
 * @param snake is a structure that hold information we need about snake such as direction x_move,y_move and so on
 * @param change_in_direciton tells us in which way snake want to move and has values -1,0,1
 * @param enemy is enemy structure to chekc if colision with enemy will occure
*/
bool check_possible_collision(snake_t *snake,int change_in_direction,snake_t *enemy)
{
	int x_possible_move = 0;
	int y_possible_move = 0;
	int possible_change = snake->direction + change_in_direction;
	if(possible_change > 4)
	{
		possible_change = 1;
	}
	if(possible_change < 1)
	{
		possible_change = 4;
	}
	switch(possible_change)
	{
		case 1:
			x_possible_move = 0;
			 y_possible_move = 1;
			break;
		case 2:
			x_possible_move = 1;
			 y_possible_move = 0;
			break;
		case 3:
			x_possible_move = 0;
			 y_possible_move = -1;
			break;
		case 4:
			x_possible_move = -1;
			 y_possible_move = 0;
			break;
	}
	int new_x = (snake->first)->x_axis + x_possible_move;
	int new_y = (snake->first)->y_axis + y_possible_move;
	tile_t *temporary = snake->first;
	while(temporary != NULL)
	{
			if(new_x == temporary->x_axis && temporary->y_axis == new_y)
			{
				return true;
			}
			temporary = temporary->next;
	}
	temporary = enemy->first;
	while(temporary != NULL)
	{
			if(new_x == temporary->x_axis && temporary->y_axis == new_y)
			{
				return true;
			}
			temporary = temporary->next;
	}
	return false;
}
/**
 * Function that we use to change the direction of snake
 * If it is controled by human it just changes direction by user input
 * If it is controled by AI it takes all its current position,direction and choose where to move to be closer to food
 * @param snake is a structure that we use to set direction and to change some values
 * @param enemy is the other snake we use to check if it not in our way
*/
void change_direction(snake_t *snake,snake_t *enemy)
{
	bool direction_changed = false;
	if(snake->AI == true)
	{
			int to_x = (snake->first)->x_axis - point_x;
			int to_y = (snake->first)->y_axis- point_y;
			int direction = snake->direction;
			if(to_y < 0)
			{
				if(snake->direction == 1)
				{
					direction_changed = false;//do nothing
				}
				else if(snake->direction == 2)
				{
					if(check_possible_collision(snake,-1,enemy)== false)
					{
						snake->direction -=1;
						direction_changed = true;
					}
				}
				else if(snake->direction == 4)
				{
					if(check_possible_collision(snake,1,enemy)== false)
					{
						snake->direction +=1;
						direction_changed = true;
					}
				}
				else if(snake->direction == 3)
				{
					if(to_x > 0)
					{
						if(check_possible_collision(snake,-1,enemy)== false)
						{
							snake->direction  -=1;
							direction_changed = true;
						}
					}
					else
					{
						if(check_possible_collision(snake,-1,enemy)== false)
						{
							snake->direction  -=1;
							direction_changed = true;
						}
					}
				}
			}
			if(to_x < 0 && direction_changed == false)
			{
				if(snake->direction == 2)
				{
					direction_changed = false;//do nothing
				}
				else if(snake->direction == 3)
				{
					if(check_possible_collision(snake,-1,enemy)== false)
					{
						snake->direction -= 1;
						direction_changed = true;
					}
				}
				else if(snake->direction == 1)
				{
					if(check_possible_collision(snake,1,enemy)== false)
					{
						snake->direction += 1;
						direction_changed = true;
					}
				}
				else if(snake->direction == 4)
				{
					if(to_y > 0)
					{
						if(check_possible_collision(snake,-1,enemy)== false)
						{
							snake->direction  -=1;
							direction_changed = true;
						}
					}
					else
					{
						if(check_possible_collision(snake,-1,enemy)== false)
						{
							snake->direction  -= 1;
							direction_changed = true;
						}
					}
				}
			}
			if(to_y > 0 && direction_changed == false)
			{
				if(snake->direction == 3)
				{
					direction_changed = false;//do nothing
				}
				else if(snake->direction == 4)
				{
					if(check_possible_collision(snake,-1,enemy)== false)
					{
						snake->direction -= 1;
						direction_changed = true;
					}
				}
				else if(snake->direction == 2)
				{
					if(check_possible_collision(snake,1,enemy)== false)
					{
						snake->direction += 1;
						direction_changed = true;
					}
				}
				else if(snake->direction == 3)
				{
					if(to_x > 0)
					{
						if(check_possible_collision(snake,-1,enemy)== false)
						{
							snake->direction  -= 1;
							direction_changed = true;
						}
					}
					else
					{
						if(check_possible_collision(snake,-1,enemy)== false)
						{
							snake->direction  -= 1;
							direction_changed = true;
						}
					}
				}
			}
			if(to_x > 0 && direction_changed == false)
			{
				if(snake->direction == 4)
				{
					direction_changed = false;//do nothing
				}
				else if(snake->direction == 3)
				{
					if(check_possible_collision(snake,1,enemy)== false)
					{
						snake->direction += 1;
						direction_changed = true;
					}
				}
				else if(snake->direction == 1)
				{
					if(check_possible_collision(snake,-1,enemy)== false)
					{
						snake->direction -= 1;
						direction_changed = true;
					}
				}
				else if(snake->direction == 2)
				{
					if(to_y > 0)
					{
						if(check_possible_collision(snake,-1,enemy)== false)
						{
							snake->direction  -= 1;
							direction_changed = true;
						}
					}
					else
					{
						if(check_possible_collision(snake,-1,enemy)== false)
						{
							snake->direction  -= 1;
							direction_changed = true;
						}
					}
				}
			}
				
	}
	if(snake->direction > 4)
	{
		snake->direction = 1;
	}
	if(snake->direction < 1)
	{
		snake->direction = 4;
	}
	switch(snake->direction)
	{
		case 1:
			snake->x_move = 0;
			snake->y_move = 1;
			break;
		case 2:
			snake->x_move = 1;
			snake->y_move = 0;
			break;
		case 3:
			snake->x_move = 0;
			snake->y_move = -1;
			break;
		case 4:
			snake->x_move = -1;
			snake->y_move = 0;
			break;
	}
}
/**
 * main function where we start our program, calls function in order we need
 */
int main(int argc, char *argv[]) {
    
  unsigned char *mem_base; /*!<Base adress in which to write so we can  write into physical adress that we really want to write*/
  unsigned char *parlcd_mem_base;/*!<Base adress in which to write so we can write into lcd*/
  srand (time(NULL));
  int sleep_time = 500000;/*!<Variable that we use as default time between turns*/
  uint32_t val_line=5;
  int i,j;

  printf("Hello world\n");
  //sleep(1);

  /*
   * Setup memory mapping which provides access to the peripheral
   * registers region of RGB LEDs, knobs and line of yellow LEDs.
   */
  mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  unsigned char *led1 = mem_base + 0x010;/*!<Base odress of first led*/
  unsigned char *led2 = mem_base + 0x014;/*!<Base odress of second led*/
  /* If mapping fails exit with error code */
  if (mem_base == NULL)
    exit(1);
	
  struct timespec loop_delay = {.tv_sec = 0};
  for (i=0; i<30; i++) {
     *(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = val_line;
     val_line<<=1;
     printf("LED val 0x%x\n", val_line);
     
  }
  
  clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

  if (parlcd_mem_base == NULL)
    exit(1);

  parlcd_hx8357_init(parlcd_mem_base);
  point_x = 8;/*!<First coordinates of food. That its completely random*/
  point_y = 4;
  // first tile of first snake
  tile_t *first_tile = malloc(sizeof(tile_t));
  first_tile->x_axis = 1;
  first_tile->y_axis = 1;
  first_tile->next = NULL;
  
  // initializing first snake
  snake_t *snake1 = malloc(sizeof(snake_t));
  snake1->first = first_tile;
  snake1->length = 3;
  snake1->x_move = 0;
  snake1->y_move = 1;
  snake1->led = led1;
  snake1->direction = 1;
  snake1->AI = false;
  snake1->colour = 0xf800;
  // intializing second tile
  tile_t *second_tile = malloc(sizeof(tile_t));
  second_tile->x_axis =9;
  second_tile->y_axis = 8;
  second_tile->next = NULL;
  // initializing second snake
  snake_t *snake2 = malloc(sizeof(snake_t));
  snake2->first = second_tile;
  snake2->length = 3;
  snake2->x_move = 0;
  snake2->y_move = 1;
  snake2->led = led2;
  snake2->direction = 1;
  snake2->AI = true;
  snake2->colour = 0x001f;

  // set terminal in raw version and to skip read if we dont receive any char
  system ("/bin/stty raw");
  system("stty -g > ~/.stty-save");
  system("stty -icanon min 0 time 0");
while(true)
{
    // game menu - game mode or settings
    char str[]="Snake 1";
    char *ch=str;
    char str5[]="0)AI vs HUMAN";
    char *ch5=str5;
    char str6[]="1)AI vs AI";
    char *ch6=str6;
    char str7[]="2)Settings";
    char *ch7=str7;

    for (i = 0; i < 320 ; i++) {
        for (j = 0; j < 480 ; j++) {
            lcd_display[i][j] = 0x0;
    	}
    }
    draw_text(ch,7,0xf800,50,20,SCALE_PIXEL,20);
    draw_text(ch5,13,0xf800,20,100,SCALE_PIXEL,5);
    draw_text(ch6,10,0xf800,20,170,SCALE_PIXEL,5);
    draw_text(ch7,10,0xf800,20,240,SCALE_PIXEL,5);
    print_lcd(parlcd_mem_base);
	char c;
	//menu reaction to user input
	if(read(STDIN_FILENO, &c, 1) == 1)
    {
		if(c == 'q')
		{
			break;
		}
		else if(c == '0')
		{
			break;
		}
		else if(c == '1')
		{
			snake1->AI = true;
			break;
		}
		else if(c == '2')
		{
            
		    // second menu where we choose what we want to change
			while(true)
			{
                for (i = 0; i < 320 ; i++) {
                    for (j = 0; j < 480 ; j++) {
                        lcd_display[i][j] = 0x0;
                    }
                }
				char str8[]="Settings";
                char *ch8=str8;
                char str9[]="0)Level";
                char *ch9=str9;
                char str10[]="1)Colour";
                char *ch10=str10;
                char str11[]="2)Exit";
                char *ch11=str11;
                draw_text(ch8,8,0xf800,50,20,SCALE_PIXEL,20);
                draw_text(ch9,7,0xf800,20,100,SCALE_PIXEL,5);
                draw_text(ch10,8,0xf800,20,170,SCALE_PIXEL,5);
                draw_text(ch11,6,0xf800,20,240,SCALE_PIXEL,5);
                print_lcd(parlcd_mem_base);
				char d;
				if(read(STDIN_FILENO, &d, 1) == 1)
				{
					if(d == 'q' || d== '2')
					{
						break;
					}
					else if(d == '0')
					{
					    // menu where we change level - changes speed of the game
						while(true)
						{		
							for (i = 0; i < 320 ; i++) {
                                for (j = 0; j < 480 ; j++) {
                                    lcd_display[i][j] = 0x0;
                                }
							}	
                            char str12[]="Level";
                            char *ch12=str12;
                            char str13[]="0)Easy";
                            char *ch13=str13;
                            char str14[]="1)Medium";
                            char *ch14=str14;
                            char str15[]="2)Hard";
                            char *ch15=str15;
                            draw_text(ch12,5,0xf800,50,20,SCALE_PIXEL,20);
                            draw_text(ch13,6,0xf800,20,100,SCALE_PIXEL,5);
                            draw_text(ch14,8,0xf800,20,170,SCALE_PIXEL,5);
                            draw_text(ch15,6,0xf800,20,240,SCALE_PIXEL,5);
                            print_lcd(parlcd_mem_base);
							char e;
							if(read(STDIN_FILENO, &e, 1) == 1)
							{
									if(e == 'q')
									{
										break;
									}
									else if(e == '0')
									{
										sleep_time = 1000000;
										break;
										
									}
									else if(e == '1')
									{
										sleep_time = 500000;
										break;
									}
									else if(e == '2')
									{
										sleep_time = 200000;
										break;
                                    }
							}
							sleep(1);
						}
					}
					else if(d == '1')
					{ 
					    //menu where we change colour
						while(true)
						{
							for (i = 0; i < 320 ; i++) {
                                for (j = 0; j < 480 ; j++) {
                                    lcd_display[i][j] = 0x0;
                                }
							}
							char e;
							char str16[]="Colour";
                            char *ch16=str16;
                            char str17[]="0)Green";
                            char *ch17=str17;
                            char str18[]="1)Blue";
                            char *ch18=str18;
                            char str19[]="2)Red";
                            char *ch19=str19;
                            draw_text(ch16,7,0xf800,50,20,SCALE_PIXEL,20);
                            draw_text(ch17,7,0xf800,20,100,SCALE_PIXEL,5);
                            draw_text(ch18,6,0xf800,20,170,SCALE_PIXEL,5);
                            draw_text(ch19,5,0xf800,20,240,SCALE_PIXEL,5);
                            print_lcd(parlcd_mem_base);
							if(read(STDIN_FILENO, &e, 1) == 1)
							{
									if(e == 'q')
									{
										break;
									}
									else if(e == '0')
									{
										snake1->colour = 0x07e0;
										break;
									}
									else if(e == '1')
									{
										snake1->colour = 0x001f;
										break;
									}
									else if(e == '2')
									{
										snake1->colour = 0xf800;
										break;
                                    }
							}
							sleep(1);
						}
					}
				}
			}
		}
	}
	sleep(1);
}
// start of snake game
while(true)
{
    // ssh read
	char c;
	if(read(STDIN_FILENO, &c, 1) == 1)
    {
		if(c == 'q')
		{
			break;
		}
		if(snake1->AI == false)
		{
			if(c == 'a')
			{
				snake1->direction -=1;
			}
			else if(c == 's')
			{
				snake1->direction +=1;
			}	
			change_direction(snake1,snake2);
		}
    }
    // pinrts green colour onto led - everything is ok
	write_led(snake1->led,0x0000ff00);
	write_led(snake2->led,0x0000ff00);
	//sets display for play area and draws food
	for(i = 0; i < 320;i++)
	{
		for(j= 0; j < 480;j++)
		{
			if(i < LINE_SIZE || (i < 320- TEXT_SPACE && i >=320 -TEXT_SPACE- LINE_SIZE))
			{
				lcd_display[i][j] = 0xf800;
			}
			else if(j < LINE_SIZE || j >= 480- LINE_SIZE)
			{
				lcd_display[i][j] = 0xf800;
			}
			else if(i >= point_x*SIZE_SQUARE + LINE_SIZE && i < point_x*SIZE_SQUARE+ SIZE_SQUARE + LINE_SIZE&& j>= point_y *SIZE_SQUARE + LINE_SIZE&& j < point_y*SIZE_SQUARE +SIZE_SQUARE + LINE_SIZE)
			{
				lcd_display[i][j] = 0xffff;
			}
			else
			{
				lcd_display[i][j]= 0;
			}
		}
	}
	//make next move
	change_direction(snake1,snake2);
	next_move(snake1);
	if(check_collision(snake1,snake1->first,11,22,false) == true)
	{
		number_of_winner = 2;
		end_screen(snake1);
		break;
	}
	if(check_collision(snake2,snake1->first,11,22,true) == true)
	{
		number_of_winner = 2;
		end_screen(snake1);
		break;
	}
	check_food(snake1);
	write_snake(snake1);
	//second snake
	change_direction(snake2,snake1);
	next_move(snake2);
	if(check_collision(snake2,snake2->first,11,22,false) == true)
	{
		number_of_winner = 1;
		end_screen(snake2);
		break;
	}
	if(check_collision(snake1,snake2->first,11,22,true) == true)
	{
		number_of_winner = 1;
		end_screen(snake2);
		break;
	}
	check_food(snake2);
	write_snake(snake2);
	//writes actual score on lcd
	printf("\n");
	char str1[] = "H1: ";
	sprintf(&str1[4],"%d",snake1->length);
	char *ch=str1;
	draw_text(ch,6,0xf800,20,270,3,10);

	char str2[] = "H2: ";
	sprintf(&str2[4],"%d",snake2->length);
	char *ch1 = str2;
	draw_text(ch1,6,0xf800,250,270,3,10);
	
	print_lcd(parlcd_mem_base);
	usleep(sleep_time);

}
// prints who won after game has ended
for(int i = 0; i < 320;i++)
{
	for(int j = 0 ; j < 480;j++)
	{
		lcd_display[i][j] = 0x0;
		}
}
char str2[] = "Winner is :  ";
char *ch2=str2;
draw_text(ch2,11,0xf800,40,40,5,10);
char str3[] = "Player:  ";
sprintf(&str3[7],"%d",number_of_winner);
char *ch3 = str3;
draw_text(ch3,9,0xf800,40,140,5,10);
print_lcd(parlcd_mem_base);

printf("GOODBYE\n");
 system("stty $(cat ~/.stty-save)");
return 0;
}
